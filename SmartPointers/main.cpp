#include <iostream>
#include "SmartPointers.h"
#include <memory>

using namespace std;
class A
{
public:
	int a = 10;
};

class B
{
public:
	int a = 11;
};
int main()
{
	while (true)
	{
		cout << "!" << endl;

		//UniquePtr<A> up1;
		//UniquePtr<A> up2(new A);
		//UniquePtr<A> up3 = move(up1);
		//UniquePtr<A> up4 = move(up2);

		//SharedPtr<A> sp1;
		//SharedPtr<A> sp2(new A);
		//SharedPtr<A> sp3 = sp1;
		//sp3 = sp2;
		//SharedPtr<A> sp4 = move(up3);
		//sp4 = move(up4);

		//WeakPtr<A> wp1 = sp1;
		//WeakPtr<A> wp2 = sp2;
		WeakPtr<A> wp3;
		{
			SharedPtr<A> sp5(new A);
			wp3 = sp5;
		}
		SharedPtr<A> lock = wp3.Lock();
		lock = nullptr;

		cout << (lock == nullptr ? "null" : "ptr") << endl;
		//cout << (wp3.Lock() == nullptr ? "null" : "ptr") << endl;
	}

	std::cout << "Hello World!\n";
}


