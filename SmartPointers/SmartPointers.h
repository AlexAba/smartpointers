#pragma once
#include <iostream>

using namespace std;

template<typename T> class UniquePtr;
template<typename T> class SharedPtr;
template<typename T> class WeakPtr;

struct Counter
{
public:
	unsigned int use = 0;
	unsigned int weak = 0;
};

template<typename T> class UniquePtr
{
public:
	UniquePtr()
	{
		ptr = nullptr;
	}
	UniquePtr(nullptr_t) : UniquePtr()
	{
	}
	explicit UniquePtr(T* pointer)
	{
		ptr = pointer;
	}
	UniquePtr(const UniquePtr& inPtr) = delete;
	UniquePtr(UniquePtr&& inPtr)
	{
		ptr = inPtr.ptr;
		inPtr.ptr = nullptr;
	}
	~UniquePtr()
	{
		if (ptr)
		{
			delete ptr;
		}
	}

	T* GetRawPtr() const
	{
		return ptr;
	}
	void Release()
	{
		ptr = nullptr;
	}

	UniquePtr& operator=(const UniquePtr& ptr) = delete;
	UniquePtr& operator=(UniquePtr&& inPtr) noexcept
	{
		ptr = inPtr.ptr;
		inPtr.ptr = nullptr;
		return *this;
	}
	UniquePtr& operator=(nullptr_t) noexcept
	{
		ptr = nullptr;
		return *this;
	}
	T& operator*() const
	{
		return *GetRawPtr();
	}
	T* operator->() const
	{
		return GetRawPtr();
	}
	explicit operator bool() const
	{
		return ptr != nullptr;
	}

private:
	T* ptr;
};

template<typename T> class BasePtr
{
public:
	BasePtr(const BasePtr& ptr) = delete;
	BasePtr& operator=(const BasePtr& ptr) = delete;

protected:
	BasePtr() = default;

	T* ptr = nullptr;
	Counter* counter = nullptr;

	void WeaklyConstructFrom(const BasePtr<T>& other)
	{
		ptr = other.ptr;
		counter = other.counter;
		++counter->weak;
	}
	void ConstructFromWeak(const BasePtr<T>& other)
	{
		ptr = other.ptr;
		counter = other.counter;
		++other.counter->use;
	}
};

template<typename T> class SharedPtr : public BasePtr<T>
{

	friend WeakPtr<T>;

public:
	SharedPtr() = default;

	explicit SharedPtr(T* pointer)
	{
		if (pointer)
		{
			this->ptr = pointer;
			this->counter = new Counter;
			this->counter->use = 1;
		}
	}

	SharedPtr(const SharedPtr<T>& other)
	{
		if (other)
		{
			this->ptr = other.ptr;
			this->counter = other.counter;
			++this->counter->use;
		}
	}

	SharedPtr(SharedPtr<T>&& other) noexcept
	{
		this->ptr = other.ptr;
		this->counter = other.counter;

		other.ptr = nullptr;
		other.counter = nullptr;
	}

	SharedPtr(UniquePtr<T>&& unique)
	{
		if (unique)
		{
			this->ptr = unique.GetRawPtr();
			this->counter = new Counter;
			this->counter->use = 1;

			unique.Release();
		}
	}

	explicit SharedPtr(const WeakPtr<T>& weak)
	{
		if (weak.counter.use > 0)
		{
			this->ConstructFromWeak(weak);
		}
		else
		{
			throw "Bad weak pointer";
		}
	}

	~SharedPtr()
	{
		if (this->ptr)
		{
			--(this->counter->use);
			if (this->counter->use <= 0)
			{
				delete this->ptr;
				if (this->counter->weak <= 0)
				{
					delete this->counter;
				}
			}
		}
	}

	SharedPtr<T>& operator=(const SharedPtr<T>& other)
	{
		if (this->ptr)
		{
			--this->counter->use;
		}

		this->ptr = other.ptr;
		this->counter = other.counter;
		++(other.counter->use);
		return *this;
	}

	SharedPtr<T>& operator=(SharedPtr<T>&& other)
	{
		if (this->ptr)
		{
			--this->counter->use;
		}

		this->ptr = other.ptr;
		this->counter = other.counter;

		other.ptr = nullptr;
		other.counter = nullptr;

		return *this;
	}

	SharedPtr<T>& operator=(UniquePtr<T>&& unique)
	{
		if (this->ptr)
		{
			--this->counter->use;
		}
		else
		{
			this->counter = new Counter;
			this->counter->use = 1;
		}

		this->ptr = unique.GetRawPtr();
		unique.Release();

		return *this;
	}

	SharedPtr<T>& operator=(const WeakPtr<T>& weak)
	{
		if (weak.counter.use > 0)
		{
			this->ptr = weak.ptr;
			this->counter = weak.counter;
			this->counter.use;
		}
		else
		{
			throw "Bad weak pointer";
		}
	}

	SharedPtr<T>& operator=(nullptr_t)
	{
		this->ptr = nullptr;
		this->counter = nullptr;
	}

	T* GetRawPtr() const
	{
		return this->ptr;
	}

	explicit operator bool() const
	{
		return GetRawPtr() != nullptr;
	}

	T& operator*() const
	{
		return *GetRawPtr();
	}
	T* operator->() const
	{
		return GetRawPtr();
	}

	bool operator==(const SharedPtr<T>& right) const
	{
		return this->ptr == right.ptr;
	}
	bool operator!=(const SharedPtr<T>& right) const
	{
		return this->ptr != right.ptr;
	}

	bool operator==(nullptr_t) const
	{
		return this->ptr == nullptr;
	}
	bool operator!=(nullptr_t) const
	{
		return this->ptr != nullptr;
	}

};

template<typename T> class WeakPtr : public BasePtr<T>
{
public:
	SharedPtr<T> Lock() const
	{
		SharedPtr<T> res;
		if (this->counter && this->counter->use > 0)
		{
			res.ConstructFromWeak(*this);
		}
		return res;
	}

	constexpr WeakPtr() = default;

	WeakPtr(const SharedPtr<T>& other)
	{
		if (other)
		{
			this->WeaklyConstructFrom(other);
		}
	}

	WeakPtr(const WeakPtr<T>& other)
	{
		this->ptr = other.ptr;
		this->counter = other.counter;
		++this->counter.weak;
	}

	WeakPtr(WeakPtr<T>&& other)
	{
		this->ptr = other.ptr;
		this->counter = other.counter;

		other.ptr = nullptr;
		other.counter = nullptr;
	}

	WeakPtr<T>& operator=(const WeakPtr<T>& other)
	{
		this->ptr = other.ptr;
		this->counter = other.counter;
		++this->counter.weak;

		return *this;
	}

	WeakPtr<T>& operator=(WeakPtr<T>&& other) noexcept
	{
		this->ptr = other.ptr;
		this->counter = other.counter;

		other.ptr = nullptr;
		other.counter = nullptr;

		return *this;
	}

	~WeakPtr()
	{
		if (this->counter != nullptr)
		{
			--this->counter->weak;
			if (this->counter->weak <= 0 && this->counter->use <= 0)
			{
				delete this->counter;
			}
		}
	}
};
